<%@ page import="cz.uhk.web.servlet.Game" %>
<%@page contentType="text/html"%>
<html>
<head><title>JSP - priklad 1</title></head>
<script>
    function send(x,y,val)
    {
        var url = 'http://localhost:8080/web_war_exploded/Game?x='+x+'&y='+y+'&val='+val;
        fetch(url, {method: "POST"})
            .then(res=>{location.reload()
        });
    }

</script>
<body>
<%
    Game game = (Game) application.getAttribute("game");
    if(game == null)
    {
        game = new Game();
        application.setAttribute("game", game);
    }

    out.println("<table>");
    for (int i=0; i<20; i++ )
    {
        out.println("<tr>");
        for (int j = 0; j < 20; j++)
        {
            out.println("<td>");
            if(game.getBoard()[i][j]==null)
                out.println("<button onclick='send("+i+","+j+",\"O\")'>HRAJ</button>");
            else
                out.println(game.getBoard()[i][j]);
            out.println("</td>");
        }
        out.println("</tr>");
    }

    out.println("</table>");

//    for(int i=1; i<=10; i++) {
//        out.println("<h1>nadpis cislo "+i+"</h1>");
//    }
%>
    
</body>
</html>
