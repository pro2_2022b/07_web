package cz.uhk.web.servlet;

import javax.servlet.*;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;
import java.util.Locale;

@WebServlet("/Servlet1")
public class Servlet1 extends HttpServlet {
  
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, java.io.IOException {
        response.setLocale(new Locale("cs","CZ"));
        response.setContentType("text/html");
        java.io.PrintWriter out = response.getWriter();

        out.println("<html>");
        out.println("<head>");
        out.println("<title>Servlet1</title>");
        out.println("</head>");
        out.println("<body>");
        out.print("Priklad spolecne obsluhy metody GET a POST");
        out.println("</body>");
        out.println("</html>");
         
        out.close();
    }
    
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, java.io.IOException {
        doGet(request,response);
    }
    
}
