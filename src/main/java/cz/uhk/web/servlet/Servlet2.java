package cz.uhk.web.servlet;
import javax.servlet.*;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;
import java.util.*;

/**
 * Servlet zobrazujici hlavicky a promnenne requestu
 * @author Tomas Kozel
 *
 */
@WebServlet("/Servlet2")
public class Servlet2 extends HttpServlet {
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, java.io.IOException {
        String var, val;
        
        response.setLocale(new Locale("cs","CZ"));
        response.setContentType("text/html");
        java.io.PrintWriter out = response.getWriter();

        out.println("<html>");
        out.println("<head>");
        out.println("<title>Servlet 2</title>");
        out.println("<meta http-equiv=\"Content-Type\" content=\"text/html; "
                  +"charset=utf-8\">");
        out.println("</head>");
        out.println("<body>");

        out.println("<H1>Seznam hlavicek requestu:</H1>");
        Enumeration<String> vars = request.getHeaderNames();
        while (vars.hasMoreElements()) {
            var = (String) vars.nextElement();
            val = request.getHeader(var);
            out.println("<b>"+var+"</b> = "+val+"<br>");
        }
        
        out.println("<H1>Seznam promennych :</H1>");
        vars = request.getParameterNames();
        while (vars.hasMoreElements()) {
            var = (String) vars.nextElement();
            val = request.getParameter(var);
            out.println("<b>"+var+"</b> = "+val+"<br>");
        }
        
        out.println("</body>");
        out.println("</html>");
         
        out.close();
    }
    
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, java.io.IOException {
        doGet(request,response);
    }
    
}
