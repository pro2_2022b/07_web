package cz.uhk.web.servlet;

import javax.servlet.*;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;
import java.util.*;
import java.io.*;

/**
 * Servlet zobrazujici cas na jednoduche 
 * HTML strance
 * @author Tomas Kozel
 *
 */
@WebServlet("/TimeServlet")
public class TimeServlet extends HttpServlet {
    TimeService tm = null;
    
    public void init(ServletConfig config) throws ServletException {
        super.init(config);
        //tm = new TimeService();
    }
    
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        response.setContentType("text/html");
        response.setLocale(new Locale("cs","CZ"));
        
        tm = new TimeService();
        PrintWriter out = response.getWriter();
        out.println("<HEAD><TITLE>TimeServlet</TITLE></HEAD>");
        out.println("<BODY>");
        out.println("<H1>TimeServlet</H1>");
        out.println("(Zkuste si reload a porovnejte cas ?!)<hr>");
        out.println(tm.getTimeString());

        out.println("</BODY>");
    }
    
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        doGet(request, response);
    }
    
}
