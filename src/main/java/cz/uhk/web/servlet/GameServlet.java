package cz.uhk.web.servlet;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Locale;

@WebServlet("/Game")
public class GameServlet extends HttpServlet {
  
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, java.io.IOException {
        response.setLocale(new Locale("cs","CZ"));
        response.setContentType("text/html");
        java.io.PrintWriter out = response.getWriter();
        Game game = (Game) getServletContext().getAttribute("game");
        if(game == null)
        {
            out.println("EMPTY");
        }
        else
        {
            int x = Integer.parseInt(request.getParameter("x"));
            int y = Integer.parseInt(request.getParameter("y"));
            out.println(game.getBoard()[x][y]);
        }
        out.close();
    }
    
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, java.io.IOException
    {
        String x =request.getParameter("x");
        String y =request.getParameter("y");
        String val = request.getParameter("val");
        Game game = (Game) getServletContext().getAttribute("game");
        if(game == null)
        {
            game = new Game();
            getServletContext().setAttribute("game",game);
        }
        game.getBoard()[Integer.parseInt(x)][Integer.parseInt(y)] = val;
        game.setCrossTurn(!game.isCrossTurn());
        response.getWriter().close();
    }
    
}
